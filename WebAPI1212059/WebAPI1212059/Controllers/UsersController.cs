﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212059.Models;
namespace WebAPI1212059.Controllers
{
    public class UsersController : ApiController
    {
        static readonly IUsersRepository repository = new UsersRepository();

        public IEnumerable<Users> GetAllUsers1212059()
        {
            return repository.GetAll1212059();
        }
     
        public Users GetUsers(int id)
        {
            Users item = repository.Get1212059(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
            
        public Users PostUsers(Users item)
        {
            item = repository.Add1212059(item);
            return item;
        }
        public void PutUsers(int id, Users users)
        {
            users.Id = id;
            if (!repository.Update1212059(users))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        public void DeleteUsers(int id)
        {
            Users item = repository.Get1212059(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove1212059(id);
        }
    }
}
