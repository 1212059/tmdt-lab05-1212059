﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212059.Models;
namespace WebAPI1212059.Controllers
{
    public class ProductsController : ApiController
    {
        static readonly IProductRepository repository = new ProductRepository();
        [HttpGet]
        [ActionName("GET")]
        public IEnumerable<Product> GetProduct()
        {
            return repository.GetAll1212059();
        }
        [HttpGet]
        [ActionName("GET")]
        public Product GetProduct1212059(int id)
        {
            Product item = repository.Get1212059(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
        public IEnumerable<Product> GetProductsByCategory(string category)
        {
            return repository.GetAll1212059().Where(
                p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        }
        public Product PostProduct(Product item)
        {
            item = repository.Add1212059(item);
            return item;
        }
        public void PutProduct(int id, Product product)
        {
            product.Id = id;
            if (!repository.Update1212059(product))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        [HttpDelete]

        public void DeleteProduct(int id)
        {
            Product item = repository.Get1212059(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove1212059(id);
        }
    }
}
