﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212059.Models
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        private int _nextId = 1;

        public ProductRepository()
        {
            Add1212059(new Product { Name = "Tomato soup", Category = "Groceries", Price = 1.39M });
            Add1212059(new Product { Name = "Yo-yo", Category = "Toys", Price = 3.75M });
            Add1212059(new Product { Name = "Hammer", Category = "Hardware", Price = 16.99M });
        }

        public IEnumerable<Product> GetAll1212059()
        {
            return products;
        }

        public Product Get1212059(int id)
        {
            return products.Find(p => p.Id == id);
        }

        public Product Add1212059(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }

        public void Remove1212059(int id)
        {
            products.RemoveAll(p => p.Id == id);
        }

        public bool Update1212059(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = products.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            products.RemoveAt(index);
            products.Add(item);
            return true;
        }
    }
}