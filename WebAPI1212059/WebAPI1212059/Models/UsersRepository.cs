﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WebAPI1212059.Models
{
    public class UsersRepository : IUsersRepository
    {
        private List<Users> Users = new List<Users>();
   

        public UsersRepository()
        {
            Add1212059(new Users { Id = 1212059,Username = "1212059" });
            Add1212059(new Users { Id = 2,Username = "NguyenVanA" });
            Add1212059(new Users { Id = 3,Username = "NguyenThiB" });
        }

        public IEnumerable<Users> GetAll1212059()
        {
            return Users;
        }

        public Users Get1212059(int id)
        {
            return Users.Find(p => p.Id == id);
        }

     
        public Users Add1212059(Users item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            
            Users.Add(item);
            return item;
        }

        public void Remove1212059(int id)
        {
            Users.RemoveAll(p => p.Id == id);
        }

        public bool Update1212059(Users item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = Users.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            Users.RemoveAt(index);
            Users.Add(item);
            return true;
        }
    }
}