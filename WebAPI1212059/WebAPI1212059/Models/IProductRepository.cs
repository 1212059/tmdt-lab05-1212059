﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212059.Models
{
    interface IProductRepository
    {
        IEnumerable<Product> GetAll1212059();
        Product Get1212059(int id);
        Product Add1212059(Product item);
        void Remove1212059(int id);
        bool Update1212059(Product item);
    }
}

