﻿using System.Web;
using System.Web.Mvc;

namespace WebAPI1212059
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}